namespace Domian.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Book
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Title { get; set; }

        public int? AutorId { get; set; }

        public int? EditorId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? Date_Publish { get; set; }

        [StringLength(10)]
        public string Edition { get; set; }

        public double? Cost { get; set; }

        public double? Price_Minorita_Suggest { get; set; }

        public int ValorizationId { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string Description { get; set; }

        public virtual Autor Autor { get; set; }

        public virtual Editor Editor { get; set; }

        public virtual Valorization Valorization { get; set; }
    }
}
