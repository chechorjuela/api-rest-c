namespace Domian.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class BuyBook
    {
        public int Id { get; set; }

        public int? IdClient { get; set; }

        public int? IdEmployed { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateBuy { get; set; }

        public double? PriceBook { get; set; }

        public virtual Cliente Cliente { get; set; }

        public virtual Employed Employed { get; set; }
    }
}
