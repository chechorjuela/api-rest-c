namespace Domian.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Entities : DbContext
    {
        public Entities()
            : base("name=Entities")
        {
        }

        public virtual DbSet<Autor> Autors { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<BuyBook> BuyBooks { get; set; }
        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<Editor> Editors { get; set; }
        public virtual DbSet<Employed> Employeds { get; set; }
        public virtual DbSet<Valorization> Valorizations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Autor>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Autor>()
                .Property(e => e.LastName)
                .IsFixedLength();

            modelBuilder.Entity<Book>()
                .Property(e => e.Title)
                .IsFixedLength();

            modelBuilder.Entity<Book>()
                .Property(e => e.Edition)
                .IsFixedLength();

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Cliente>()
                .Property(e => e.LastName)
                .IsFixedLength();

            modelBuilder.Entity<Cliente>()
                .Property(e => e.Email)
                .IsFixedLength();

            modelBuilder.Entity<Cliente>()
                .HasMany(e => e.BuyBooks)
                .WithOptional(e => e.Cliente)
                .HasForeignKey(e => e.IdClient);

            modelBuilder.Entity<Editor>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Employed>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Employed>()
                .Property(e => e.LastName)
                .IsFixedLength();

            modelBuilder.Entity<Employed>()
                .HasMany(e => e.BuyBooks)
                .WithOptional(e => e.Employed)
                .HasForeignKey(e => e.IdEmployed);

            modelBuilder.Entity<Valorization>()
                .Property(e => e.Name)
                .IsFixedLength();

            modelBuilder.Entity<Valorization>()
                .HasMany(e => e.Books)
                .WithRequired(e => e.Valorization)
                .WillCascadeOnDelete(false);
        }
    }
}
