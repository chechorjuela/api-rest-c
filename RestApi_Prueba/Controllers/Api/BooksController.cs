﻿using Common.Contract;
using Domian.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ApiRest.Repository.Services.Interfaces;

namespace RestApi_Prueba.Controllers.Api
{
    public class BooksController : ApiController
    {

        private BookService bookService = new BookService();

        [HttpGet]
        public async  Task<ResponseContract<List<Book>>> GetBooks()
        {
            ResponseContract<List<Book>> response = new ResponseContract<List<Book>>();
            try
            {
                response.Data = this.bookService.GetBook();
            }
            catch (Exception exp)
            {
                response.Header.Code = HttpCodes.InternalServerError;
                response.Header.Message = exp.Message;
            }

            return response;
        }

        [HttpPost]
        public async Task<ResponseContract<Book>> StoreBook(Book bookSave)
        {
            ResponseContract<Book> response = new ResponseContract<Book>();
            try
            {
                response.Data = this.bookService.StoreBook(bookSave);
            }
            catch (Exception exp)
            {
                response.Header.Code = HttpCodes.InternalServerError;
                response.Header.Message = exp.Message;
            }

            return response;
        }
    }
}
