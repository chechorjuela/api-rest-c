﻿using ApiRest.Repository.Services.Implements;
using Domian.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiRest.Repository.Services.Interfaces
{
    public class BookService : IBookService
    {
        private Entities dbEntities = new Entities(); 
        public List<Book> GetBook()
        {
            return dbEntities.Books.ToList();
        }

        public Book StoreBook(Book book)
        {
            dbEntities.Books.Add(book);
            dbEntities.SaveChanges();
            return book;
        }
    }
}
