﻿using Domian.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiRest.Repository.Services.Implements
{
    public interface IBookService
    {
        List<Book> GetBook();
        Book StoreBook(Book book);
    }
}
